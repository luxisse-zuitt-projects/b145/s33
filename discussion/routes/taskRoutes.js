const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');


// Retrieving Task
router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

/* 
	Mini-Activity
	1. Create a route for createTask controller, name your endpoint as /createTask
	2. Send Postman SS in Hangouts

*/

// Creating Task
router.post("/createTask", (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
});


// Deleting
router.delete("/deleteTask/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(
		resultFromController => res.send (
			resultFromController))
})


// Updating
router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})





// START OF S33 ACTIVITY

// Retrieving a specific task
router.get("/getTask/:id", (req, res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});



// Changing status of a task
router.put("/updateTaskStatus/:id/complete", (req, res) => {
	taskControllers.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router;