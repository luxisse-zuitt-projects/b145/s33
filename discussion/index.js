// Creating a simple server in Express.js

const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes');

const app = express();
const port = 4000;


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// Connecting MongoDB
mongoose.connect("mongodb+srv://luxisse:pr0wdmon@b145.gxg3t.mongodb.net/session33?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "There is an error with the connection"))
db.once("open", () => console.log("Successfully connected to the database"))



app.use('/tasks', taskRoutes)

app.listen(port, () => console.log(`Server running at port ${port}`));