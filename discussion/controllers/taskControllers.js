const Task = require('../models/taskSchema');

// Retrieving all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	});
};


// Creating a task
module.exports.createTask = (reqBody) => {
	
	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((task, err) => {
		if(err){
				console.log(err)
				return false
		} else {
				return task
		}
	})
}


// Deleting Task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
				console.log(err)
				return false
		} else {
				return removedTask
		}
	})
}


// Updating Task
module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
				console.log(err)
				return false
		} 

		result.name = reqBody.name
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
						console.log(saveErr)
						return false
				} else {
						return updatedTask
				}
			})
	})
};





// START OF S33 ACTIVITY

// Retrieve specific task
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	});
};



// Changing status of a task
module.exports.updateTaskStatus = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
				console.log(err)
				return false
		} 

		result.status = reqBody.status
			return result.save().then((updatedTaskStatus, saveErr) => {
				if(saveErr){
						console.log(saveErr)
						return false
				} else {
						return updatedTaskStatus
				}
			})
	})
};